package Section1;

/*
    Каждая переменная имеет тип, имя и значение. Тип определяет,
    какую информацию может хранить переменная или диапазон допустимых значений.
 */
public class Lesson_2_Variables {
    public static void main(String[] args){
        // В этом выражении мы объявляем переменную x типа int. То есть x будет хранить некоторое число не больше 4 байт.
        int x;
        x = 10;
        System.out.println(x);
//        test1();
//        Constant
        final int y;
        y = 15;
        System.out.println(y);
    }



    public static void test1(){
        /*
            Если мы не присвоим переменной значение до ее использования,
            то мы можем получить ошибку, например, в следующем случае:
        */
        int y;
//        System.out.println(y);
    }
}
