package Section1;

/*
    Одной из основных особенностей Java является то, что данный язык является строго типизированным.
    А это значит, что каждая переменная и константа представляет определенный тип и данный тип строго определен.
    Тип данных определяет диапазон значений, которые может хранить переменная или константа.
 */
public class Lesson_3_data_types {
    public static void main(String[] args){
        data_types();
    }

    public static void data_types(){

        // boolean: хранит значение true или false
        boolean isActive = false;
        boolean isAlive = true;
        // to get type of variable
        System.out.println("isAlive - " + ((Object)isAlive).getClass().getName());

        // byte: хранит целое число от -128 до 127 и занимает 1 байт
        byte a = 3;
        byte b = 8;
        System.out.println("a - " + ((Object)a).getClass().getName());


        // short: хранит целое число от -32768 до 32767 и занимает 2 байта
        short as = 33;
        short bs = 88;
        System.out.println("as - " + ((Object)as).getClass().getName());


        // int: хранит целое число от -2147483648 до 2147483647 и занимает 4 байта
        int ai = 333;
        int bi = 888;
        System.out.println("ai - " + ((Object)ai).getClass().getName());


        // long: хранит целое число от –9 223 372 036 854 775 808 до 9 223 372 036 854 775 807 и занимает 8 байт
        long al = 2147483649L;
        long bl = 8147483649L;
        System.out.println("al - " + ((Object)al).getClass().getName());


        // double: хранит число с плавающей точкой от ±4.9*10-324 до ±1.7976931348623157*10308 и занимает 8 байт
        double ad = 333.3;
        double bd = 8888.8;
        System.out.println("ad - " + ((Object)ad).getClass().getName());


        // float: хранит число с плавающей точкой от -3.4*1038 до 3.4*1038 и занимает 4 байта
        float af = 3.3F;
        float bf = 8.8f;
        System.out.println("af - " + ((Object)af).getClass().getName());


        // char: хранит одиночный символ в кодировке UTF-16 и занимает 2 байта, поэтому диапазон хранимых значений от 0 до 65535



        // Также целые числа поддерживают разделение разрядов числа с помощью знака подчеркивания:
        int x = 123_456;
        int y = 234_567__789;
        /*
            printf() uses the java.util.Formatter class to parse the format string and generate the output.
            Format Specifiers
                Let’s look at the available format specifiers available for printf:

                %c character
                %d decimal (integer) number (base 10)
                %e exponential floating-point number
                %f floating-point number
                %i integer (base 10)
                %o octal number (base 8)
                %s String
                %u unsigned decimal (integer) number
                %x number in hexadecimal (base 16)
                %t formats date/time
                %% print a percent sign
                \% print a percent sign

                Note: %n or \n are used as line separators in printf().

                for more information:
                https://www.digitalocean.com/community/tutorials/java-printf-method
         */
        System.out.printf("%d - %d%n", x,y);



        /*
            При присвоении переменной типа float дробного литерала с плавающей точкой,
            например, 3.1, 4.5 и т.д., Java автоматически рассматривает этот литерал как значение типа double.
            И чтобы указать, что данное значение должно рассматриваться как float, нам надо использовать суффикс f:
         */
        float fl = 30.6f;
        double db = 30.6;
        /*
        И хотя в данном случае обе переменных имеют практически одно значения,
        но эти значения будут по-разному рассматриваться и будут занимать разное место в памяти.
         */
        System.out.printf("fl - %s%ndb - %s%n", ((Object)fl).getClass().getName(),
                ((Object)db).getClass().getName());



        /*
            Символы и строки
            В качестве значения переменная символьного типа получает одиночный символ,
            заключенный в одинарные кавычки: char ch='e';. Кроме того, переменной символьного типа также можно
            присвоить целочисленное значение от 0 до 65535. В этом случае переменная опять же будет хранить символ,
            а целочисленное значение будет указывать на номер символа в таблице символов Unicode (UTF-16). Например:
         */
        char ch=102; // символ 'f'
        System.out.println(ch);



        // Текстовые блоки, которые появились в JDK15, позволяют упростить написание многострочного текста:
        String text = """
                Вот мысль, которой весь я предан,
                Итог всего, что ум скопил.
                Лишь тот, кем бой за жизнь изведан,
                Жизнь и свободу заслужил.
                """;
        System.out.println(text);
    }
}
